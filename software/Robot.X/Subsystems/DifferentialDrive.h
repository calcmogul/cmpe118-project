#ifndef CMPE118_PROJECT_ROBOT_X_SUBSYSTEMS_DIFFERENTIALDRIVE_H_
#define CMPE118_PROJECT_ROBOT_X_SUBSYSTEMS_DIFFERENTIALDRIVE_H_

#include "../ES_Configure.h"

#include <stdint.h>

#include <ES_Events.h>

#include "Hardware/HBridge.h"

/**
 * Call HAL_Init() before initializing this class.
 */
typedef struct {
    HBridge* leftMotor;
    HBridge* rightMotor;
} DifferentialDrive;

/**
 * Initialize differential drive using left and right H bridges.
 *
 * @param inst       DifferentialDrive instance
 * @param leftMotor  H bridge for left side of robot
 * @param rightMotor H bridge for right side of robot
 */
void DifferentialDrive_Init(
        DifferentialDrive* inst, HBridge* leftMotor, HBridge* rightMotor);

/**
 * Set speed of robot sides.
 *
 * The commanded voltage for the maximum speed is 8V.
 *
 * @param leftSpeed  -1000 for full reverse and 1000 for full forward
 * @param rightSpeed -1000 for full reverse and 1000 for full forward
 */
void DifferentialDrive_Drive(int32_t leftSpeed, int32_t rightSpeed);

// Service base class
uint8_t DifferentialDrive_Service_Init(uint8_t priority);
uint8_t DifferentialDrive_Service_Post(ES_Event event);
ES_Event DifferentialDrive_Service_Run(ES_Event event);

#endif // CMPE118_PROJECT_ROBOT_X_SUBSYSTEMS_DIFFERENTIALDRIVE_H_

#ifndef CMPE118_PROJECT_ROBOT_X_SUBSYSTEMS_SHOOTER_H_
#define CMPE118_PROJECT_ROBOT_X_SUBSYSTEMS_SHOOTER_H_

#include "../ES_Configure.h"

#include <stdbool.h>
#include <stdint.h>

#include <ES_Events.h>

#include "Hardware/HBridge.h"
#include "Hardware/RCServo.h"

/**
 * Call HAL_Init() before initializing this class.
 */
typedef struct {
    HBridge* flywheel;
    RCServo* indexer;
} Shooter;

/**
 * Initialize elevator differential drive using left and right H bridges.
 *
 * @param inst     Shooter instance
 * @param flywheel H bridge for the elevator motor
 * @param indexer  RC servo for ball indexer
 */
void Shooter_Init(Shooter* inst, HBridge* flywheel, RCServo* indexer);

/**
 * Returns true if flywheel is on.
 */
bool Shooter_IsRunning(void);

// Service base class
uint8_t Shooter_Service_Init(uint8_t priority);
uint8_t Shooter_Service_Post(ES_Event event);
ES_Event Shooter_Service_Run(ES_Event event);

#endif // CMPE118_PROJECT_ROBOT_X_SUBSYSTEMS_SHOOTER_H_

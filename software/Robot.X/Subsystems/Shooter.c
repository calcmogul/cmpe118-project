#include "Shooter.h"

#include <stdbool.h>

#include <ES_Framework.h>
#include <ES_TattleTale.h>

#include "../TimerDefs.h"

typedef enum {
    kInit, // Initial state
    kFlywheelOff,
    kFlywheelOn,
    kFeedingBall,
    kWaitForElevator,
} ShooterState_t;

static const char* StateNames[] = {
    "kInit",
    "kFlywheelOff",
    "kFlywheelOn",
    "kFeedingBall",
    "kWaitForElevator",
};

static ShooterState_t state = kInit;

static uint8_t m_priority;

static Shooter* m_inst;

void Shooter_Init(Shooter* inst, HBridge* flywheel, RCServo* indexer) {
    m_inst = inst;
    m_inst->flywheel = flywheel;
    m_inst->indexer = indexer;
}

bool Shooter_IsRunning(void) { return state != kInit && state != kFlywheelOff; }

uint8_t Shooter_Service_Init(uint8_t priority) {
    ES_Event event;

    m_priority = priority;

    // Post the initial transition event
    event.EventType = ES_INIT;
    return ES_PostToService(m_priority, event);
}

uint8_t Shooter_Service_Post(ES_Event event) {
    return ES_PostToService(m_priority, event);
}

ES_Event Shooter_Service_Run(ES_Event event) {
    static uint8_t shots = 0;

    bool makeTransition = false;
    ShooterState_t nextState;

#ifdef USE_TATTLETALE
    ES_AddTattlePoint(__FUNCTION__, StateNames[state], event);
#endif

    switch (state) {
        case kInit:
            switch (event.EventType) {
                case ES_INIT:
                    nextState = kFlywheelOff;
                    makeTransition = true;
                    break;
            }
            break;
        case kFlywheelOff:
            switch (event.EventType) {
                case ES_ENTRY:
                    // Stop flywheel
                    HBridge_Set(m_inst->flywheel, 0);

                    // Set indexer back to get a new ball
                    RCServo_Set(m_inst->indexer, 0);
                    break;
                case CMD_SHOOTER_SHOOT:
                    nextState = kFlywheelOn;
                    makeTransition = true;
                    break;
            }
            break;
        case kFlywheelOn:
            switch (event.EventType) {
                case ES_ENTRY:
                    HBridge_Set(m_inst->flywheel, -1000);
                    ES_Timer_InitTimer(TIMER_SHOOTER, 500);
                    break;
                case ES_EXIT:
                    ES_Timer_StopTimer(TIMER_SHOOTER);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_SHOOTER) {
                        nextState = kFeedingBall;
                        makeTransition = true;
                    }
                    break;
            }
            break;
        case kFeedingBall:
            switch (event.EventType) {
                case ES_ENTRY:
                    // Push new ball into flywheel
                    RCServo_Set(m_inst->indexer, 1000);

                    ES_Timer_InitTimer(TIMER_SHOOTER, 600);
                    break;
                case ES_EXIT:
                    ES_Timer_StopTimer(TIMER_SHOOTER);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_SHOOTER) {
                        shots++;

                        // If 3 shots, move elevator up for Ren Ship
                        if (shots == 3) {
                            Elevator_Service_Post(
                                    (ES_Event){ CMD_ELEVATOR_TOP, 0 });
                            nextState = kWaitForElevator;
                        } else {
                            ES_PostAll((ES_Event){ DONE_SHOOTER_SHOOT, 0 });
                            nextState = kFlywheelOff;
                        }
                        makeTransition = true;
                    }
                    break;
            }
            break;
        case kWaitForElevator:
            switch (event.EventType) {
                case ES_ENTRY:
                    // Stop flywheel
                    HBridge_Set(m_inst->flywheel, 0);

                    // Set indexer back to get a new ball
                    RCServo_Set(m_inst->indexer, 0);
                    break;
                case DONE_ELEVATOR_TOP:
                    ES_PostAll((ES_Event){ DONE_SHOOTER_SHOOT, 0 });
                    nextState = kFlywheelOff;
                    makeTransition = true;
                    event.EventType = ES_NO_EVENT;
                    break;
            }
            break;
    }

    if (makeTransition) {
        Shooter_Service_Run((ES_Event){ ES_EXIT, 0 });
        state = nextState;
        Shooter_Service_Run((ES_Event){ ES_ENTRY, 0 });
    }

#ifdef USE_TATTLETALE
    ES_Tail(); // trace call stack end
#endif

    return event;
}

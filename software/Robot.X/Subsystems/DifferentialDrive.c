#include "DifferentialDrive.h"

#include <stdbool.h>

#include <ES_Framework.h>
#include <ES_TattleTale.h>

#include "../TimerDefs.h"

typedef enum {
    kInit, // Initial state
    kIdle,
    kRotatingCW90,
    kRotatingCCW90,
} DifferentialDriveState_t;

static const char* StateNames[] = {
    "kInit",
    "kIdle",
    "kRotatingCW90",
    "kRotatingCCW90",
};

static DifferentialDriveState_t state = kInit;

static uint8_t m_priority;

static DifferentialDrive* m_inst;

// Adds to left motor speed and subtracts from right motor speed
static const int32_t kTurnTrim = 0;

// The commanded voltage for the maximum speed.
static const int32_t kMaxDriveVoltage = 5;

// Variables for CW/CCW turns
static const int32_t kTurnSpeed = 500;
static const int32_t kTurnPeriodMs = 2400;

void DifferentialDrive_Init(
        DifferentialDrive* inst, HBridge* leftMotor, HBridge* rightMotor) {
    m_inst = inst;

    m_inst->leftMotor = leftMotor;
    m_inst->rightMotor = rightMotor;
}

void DifferentialDrive_Drive(int32_t leftSpeed, int32_t rightSpeed) {
    HBridge_SetVoltage(
            m_inst->leftMotor, kMaxDriveVoltage * leftSpeed + kTurnTrim);
    HBridge_SetVoltage(
            m_inst->rightMotor, kMaxDriveVoltage * rightSpeed - kTurnTrim);
}

uint8_t DifferentialDrive_Service_Init(uint8_t priority) {
    ES_Event event;

    m_priority = priority;

    // Post the initial transition event
    event.EventType = ES_INIT;
    return ES_PostToService(m_priority, event);
}

uint8_t DifferentialDrive_Service_Post(ES_Event event) {
    return ES_PostToService(m_priority, event);
}

ES_Event DifferentialDrive_Service_Run(ES_Event event) {
    bool makeTransition = false;
    DifferentialDriveState_t nextState;

#ifdef USE_TATTLETALE
    ES_AddTattlePoint(__FUNCTION__, StateNames[state], event);
#endif

    switch (state) {
        case kInit:
            switch (event.EventType) {
                case ES_INIT:
                    nextState = kIdle;
                    makeTransition = true;
                    break;
            }
            break;
        case kIdle:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(0, 0);
                    break;
                case CMD_DIFFDRIVE_ROTATE_CW90:
                    nextState = kRotatingCW90;
                    makeTransition = true;
                    event.EventType = ES_NO_EVENT;
                    break;
                case CMD_DIFFDRIVE_ROTATE_CCW90:
                    nextState = kRotatingCCW90;
                    makeTransition = true;
                    event.EventType = ES_NO_EVENT;
                    break;
            }
            break;
        case kRotatingCW90:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(kTurnSpeed, -kTurnSpeed);
                    ES_Timer_InitTimer(TIMER_DIFFDRIVE, kTurnPeriodMs);
                    break;
                case ES_EXIT:
                    DifferentialDrive_Drive(0, 0);
                    ES_Timer_StopTimer(TIMER_DIFFDRIVE);
                    ES_PostAll((ES_Event){ DONE_DIFFDRIVE_ROTATE_CW90, 0 });
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_DIFFDRIVE) {
                        nextState = kIdle;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
        case kRotatingCCW90:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(-kTurnSpeed, kTurnSpeed);
                    ES_Timer_InitTimer(TIMER_DIFFDRIVE, kTurnPeriodMs);
                    break;
                case ES_EXIT:
                    DifferentialDrive_Drive(0, 0);
                    ES_Timer_StopTimer(TIMER_DIFFDRIVE);
                    ES_PostAll((ES_Event){ DONE_DIFFDRIVE_ROTATE_CCW90, 0 });
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_DIFFDRIVE) {
                        nextState = kIdle;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
    }

    if (makeTransition) {
        DifferentialDrive_Service_Run((ES_Event){ ES_EXIT, 0 });
        state = nextState;
        DifferentialDrive_Service_Run((ES_Event){ ES_ENTRY, 0 });
    }

#ifdef USE_TATTLETALE
    ES_Tail(); // trace call stack end
#endif

    return event;
}

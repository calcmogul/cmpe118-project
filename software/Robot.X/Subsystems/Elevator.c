#include "Elevator.h"

#include <stdbool.h>

#include <ES_Framework.h>
#include <ES_TattleTale.h>

#include "../TimerDefs.h"

typedef enum {
    kInit, // Initial state
    kBottom,
    kGoingDown,
    kGoingUp,
    kTop,
} ElevatorState_t;

static const char* StateNames[] = {
    "kInit",
    "kBottom",
    "kGoingDown",
    "kGoingUp",
    "kTop",
};

static ElevatorState_t state = kInit;

static uint8_t m_priority;

static Elevator* m_inst;

void Elevator_Init(Elevator* inst, HBridge* elevatorMotor) {
    m_inst = inst;

    m_inst->elevatorMotor = elevatorMotor;
}

bool Elevator_AtTop(void) { return state == kTop; }

uint8_t Elevator_Service_Init(uint8_t priority) {
    ES_Event event;

    m_priority = priority;

    // Post the initial transition event
    event.EventType = ES_INIT;
    return ES_PostToService(m_priority, event);
}

uint8_t Elevator_Service_Post(ES_Event event) {
    return ES_PostToService(m_priority, event);
}

ES_Event Elevator_Service_Run(ES_Event event) {
    bool makeTransition = false;
    ElevatorState_t nextState;

    static int bumpCount = 0;
    static int startTime = 0;

    if (ES_Timer_GetTime() - startTime > 3000) {
        bumpCount = 0;
    }

#ifdef USE_TATTLETALE
    ES_AddTattlePoint(__FUNCTION__, StateNames[state], event);
#endif

    switch (state) {
        case kInit:
            switch (event.EventType) {
                case ES_INIT:
                    nextState = kBottom;
                    makeTransition = true;
                    break;
            }
            break;
        case kBottom:
            switch (event.EventType) {
                case ES_ENTRY:
                    HBridge_Set(m_inst->elevatorMotor, 0);
                    break;
                case CMD_ELEVATOR_TOP:
                    nextState = kGoingUp;
                    makeTransition = true;
                    event.EventType = ES_NO_EVENT;
                    break;
                case EVENT_BUMPER:
                    bumpCount++;
                    if (bumpCount == 1) {
                        startTime = ES_Timer_GetTime();
                    } else if (bumpCount >= 7) {
                        bumpCount = 0;
                        nextState = kGoingDown;
                        makeTransition = true;
                    }
                    event.EventType = ES_NO_EVENT;
                    break;
            }
            break;
        case kGoingDown:
            switch (event.EventType) {
                case ES_ENTRY:
                    HBridge_Set(m_inst->elevatorMotor, 1000);
                    ES_Timer_InitTimer(TIMER_ELEVATOR, 14750);
                    break;
                case ES_EXIT:
                    HBridge_Set(m_inst->elevatorMotor, 0);
                    ES_Timer_StopTimer(TIMER_ELEVATOR);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_ELEVATOR) {
                        nextState = kBottom;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
        case kGoingUp:
            switch (event.EventType) {
                case ES_ENTRY:
                    HBridge_Set(m_inst->elevatorMotor, -1000);
                    ES_Timer_InitTimer(TIMER_ELEVATOR, 15000);
                    break;
                case ES_EXIT:
                    HBridge_Set(m_inst->elevatorMotor, 0);
                    ES_PostAll((ES_Event){ DONE_ELEVATOR_TOP, 0 });
                    ES_Timer_StopTimer(TIMER_ELEVATOR);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_ELEVATOR) {
                        nextState = kTop;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
        case kTop:
            switch (event.EventType) {
                case ES_ENTRY:
                    HBridge_Set(m_inst->elevatorMotor, 0);
                    break;
            }
            break;
    }

    if (makeTransition) {
        Elevator_Service_Run((ES_Event){ ES_EXIT, 0 });
        state = nextState;
        Elevator_Service_Run((ES_Event){ ES_ENTRY, 0 });
    }

#ifdef USE_TATTLETALE
    ES_Tail(); // trace call stack end
#endif

    return event;
}

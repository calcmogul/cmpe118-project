#ifndef CMPE118_PROJECT_ROBOT_X_SUBSYSTEMS_ELEVATOR_H_
#define CMPE118_PROJECT_ROBOT_X_SUBSYSTEMS_ELEVATOR_H_

#include "../ES_Configure.h"

#include <stdint.h>

#include <ES_Events.h>

#include "Hardware/HBridge.h"

/**
 * Call HAL_Init() before initializing this class.
 */
typedef struct {
    HBridge* elevatorMotor;
} Elevator;

/**
 * Initialize elevator differential drive using left and right H bridges.
 *
 * @param inst            Elevator instance
 * @param elevatorMotor   H bridge for the elevator motor
 */
void Elevator_Init(Elevator* inst, HBridge* elevatorMotor);

/**
 * Returns true if elevator is at top of travel.
 */
bool Elevator_AtTop(void);

// Service base class
uint8_t Elevator_Service_Init(uint8_t priority);
uint8_t Elevator_Service_Post(ES_Event event);
ES_Event Elevator_Service_Run(ES_Event event);

#endif // CMPE118_PROJECT_ROBOT_X_SUBSYSTEMS_ELEVATOR_H_

#ifndef CMPE118_PROJECT_ROBOT_X_HARDWARE_DIGITALOUTPUT_H_
#define CMPE118_PROJECT_ROBOT_X_HARDWARE_DIGITALOUTPUT_H_

#include <stdbool.h>
#include <stdint.h>

#include "DIODefs.h"

typedef struct {
    uint32_t port;
    uint32_t pin;
} DigitalOutput;

/**
 * Initialize digital output.
 *
 * @param inst DigitalOutput instance
 * @param port Port
 * @param pin  Pin on port
 */
void DigitalOutput_Init(DigitalOutput* inst, Port port, Pin pin);

/**
 * Sets digital output to high or low.
 *
 * @param inst  DigitalOutput instance
 * @param value True for high and false for low
 */
void DigitalOutput_Set(DigitalOutput* inst, bool value);

#endif // CMPE118_PROJECT_ROBOT_X_HARDWARE_DIGITALOUTPUT_H_

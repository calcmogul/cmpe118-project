#include "DigitalOutput.h"

#ifndef _SUPPRESS_PLIB_WARNING
#define _SUPPRESS_PLIB_WARNING
#endif

#include <IO_Ports.h>

#include "HAL.h"

void DigitalOutput_Init(DigitalOutput* inst, Port port, Pin pin) {
    HAL_Init();

    inst->port = port;
    inst->pin = pin;

    IO_PortsSetPortOutputs(port, pin);
}

void DigitalOutput_Set(DigitalOutput* inst, bool value) {
    if (value) {
        IO_PortsSetPortBits(inst->port, inst->pin);
    } else {
        IO_PortsClearPortBits(inst->port, inst->pin);
    }
}

#ifndef CMPE118_PROJECT_ROBOT_X_HARDWARE_DIODEFS_H_
#define CMPE118_PROJECT_ROBOT_X_HARDWARE_DIODEFS_H_

typedef enum {
    kPortV = 0,
    kPortW = 1,
    kPortX = 2,
    kPortY = 3,
    kPortZ = 4
} Port;

typedef enum {
    kPin3 = 0x0008,  // 0b0000 0000 0000 1000
    kPin4 = 0x0010,  // 0b0000 0000 0001 0000
    kPin5 = 0x0020,  // 0b0000 0000 0010 0000
    kPin6 = 0x0040,  // 0b0000 0000 0100 0000
    kPin7 = 0x0080,  // 0b0000 0000 1000 0000
    kPin8 = 0x0100,  // 0b0000 0001 0000 0000
    kPin9 = 0x0200,  // 0b0000 0010 0000 0000
    kPin10 = 0x0400, // 0b0000 0100 0000 0000
    kPin11 = 0x0800, // 0b0000 1000 0000 0000
    kPin12 = 0x1000, // 0b0001 0000 0000 0000
} Pin;

#endif // CMPE118_PROJECT_ROBOT_X_HARDWARE_DIODEFS_H_

#ifndef CMPE118_PROJECT_ROBOT_X_HARDWARE_RCSERVO_H_
#define CMPE118_PROJECT_ROBOT_X_HARDWARE_RCSERVO_H_

#include <stdint.h>

/**
 * Call HAL_Init() before initializing this class.
 */
typedef struct {
    uint32_t pin;
} RCServo;

typedef enum {
    RCServo_kV3,
    RCServo_kV4,
    RCServo_kW7,
    RCServo_kW8,
    RCServo_kY6,
    RCServo_kY7,
    RCServo_kZ8,
    RCServo_kZ9
} RCServoPin;

/**
 * Initialize servo on the given pin.
 */
void RCServo_Init(RCServo* inst, RCServoPin pin);

/**
 * Destroy servo.
 */
void RCServo_Destroy(RCServo* inst);

/**
 * Set position of servo.
 *
 * @param inst  RCServo instance
 * @param speed 0 for full reverse and 1000 for full forward
 */
void RCServo_Set(RCServo* inst, int32_t position);

#endif // CMPE118_PROJECT_ROBOT_X_HARDWARE_RCSERVO_H_

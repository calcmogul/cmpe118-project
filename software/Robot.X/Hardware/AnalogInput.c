#include "AnalogInput.h"

#ifndef _SUPPRESS_PLIB_WARNING
#define _SUPPRESS_PLIB_WARNING
#endif

#include <stdio.h>

#include <AD.h>

#include "HAL.h"

void AnalogInput_Init(AnalogInput* inst, AnalogInputPin pin) {
    HAL_Init();

    switch (pin) {
        case AnalogInput_kV3:
            inst->channel = AD_PORTV3;
            break;
        case AnalogInput_kV4:
            inst->channel = AD_PORTV4;
            break;
        case AnalogInput_kV5:
            inst->channel = AD_PORTV5;
            break;
        case AnalogInput_kV6:
            inst->channel = AD_PORTV6;
            break;
        case AnalogInput_kV7:
            inst->channel = AD_PORTV7;
            break;
        case AnalogInput_kV8:
            inst->channel = AD_PORTV8;
            break;
        case AnalogInput_kW3:
            inst->channel = AD_PORTW3;
            break;
        case AnalogInput_kW4:
            inst->channel = AD_PORTW4;
            break;
        case AnalogInput_kW7:
            inst->channel = AD_PORTW7;
            break;
        case AnalogInput_kW8:
            inst->channel = AD_PORTW8;
            break;
    }

    AD_AddPins(inst->channel);
}

void AnalogInput_Destroy(AnalogInput* inst) { AD_RemovePins(inst->channel); }

uint32_t AnalogInput_Get(AnalogInput* inst) {
    uint32_t sample = AD_ReadADPin(inst->channel);
    if (sample > 1023) {
        printf("AnalogInput %u not initialized\r\n", inst->channel);
        return 0;
    } else {
        return sample;
    }
}

uint32_t AnalogInput_GetVoltage(AnalogInput* inst) {
    return AnalogInput_Get(inst) * 3300 / 1023;
}

uint32_t AnalogInput_GetBatteryVoltage(void) {
    return AD_ReadADPin(BAT_VOLTAGE) * 33 * 1000 / 1023;
}

bool AnalogInput_IsNewDataReady(void) { return AD_IsNewDataReady(); }

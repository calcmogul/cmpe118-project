#ifndef CMPE118_PROJECT_ROBOT_X_HARDWARE_DIGITALINPUT_H_
#define CMPE118_PROJECT_ROBOT_X_HARDWARE_DIGITALINPUT_H_

#include <stdbool.h>
#include <stdint.h>

#include "DIODefs.h"

typedef struct {
    uint32_t port;
    uint32_t pin;
} DigitalInput;

/**
 * Initialize digital input.
 *
 * @param port Port
 * @param pin  Pin on port
 */
void DigitalInput_Init(DigitalInput* inst, Port port, Pin pin);

/**
 * Returns true if digital input is high.
 */
bool DigitalInput_Get(DigitalInput* inst);

#endif // CMPE118_PROJECT_ROBOT_X_HARDWARE_DIGITALINPUT_H_

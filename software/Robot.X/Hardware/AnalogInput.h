#ifndef CMPE118_PROJECT_ROBOT_X_HARDWARE_ANALOGINPUT_H_
#define CMPE118_PROJECT_ROBOT_X_HARDWARE_ANALOGINPUT_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct {
    uint32_t channel;
} AnalogInput;

typedef enum {
    AnalogInput_kV3,
    AnalogInput_kV4,
    AnalogInput_kV5,
    AnalogInput_kV6,
    AnalogInput_kV7,
    AnalogInput_kV8,
    AnalogInput_kW3,
    AnalogInput_kW4,
    AnalogInput_kW5,
    AnalogInput_kW6,
    AnalogInput_kW7,
    AnalogInput_kW8
} AnalogInputPin;

/**
 * Initialize analog input.
 *
 * @param port Port
 * @param pin  Pin on port
 */
void AnalogInput_Init(AnalogInput* inst, AnalogInputPin pin);

/**
 * Free analog input pin.
 */
void AnalogInput_Destroy(AnalogInput* inst);

/**
 * Returns current analog pin value [0..1023].
 */
uint32_t AnalogInput_Get(AnalogInput* inst);

/**
 * Returns current analog pin value as a voltage out of 3.3V [0..3300].
 */
uint32_t AnalogInput_GetVoltage(AnalogInput* inst);

/**
 * Returns battery voltage [0..1000] for [0V..10V].
 */
uint32_t AnalogInput_GetBatteryVoltage(void);

/**
 * Returns true if the ADC has new values since last call to Get() for any pin.
 */
bool AnalogInput_IsNewDataReady(void);

#endif // CMPE118_PROJECT_ROBOT_X_HARDWARE_ANALOGINPUT_H_

#include "HAL.h"

#ifndef _SUPPRESS_PLIB_WARNING
#define _SUPPRESS_PLIB_WARNING
#endif

#include <stdbool.h>

#include <AD.h>
#include <BOARD.h>
#include <RC_Servo.h>
#include <pwm.h>

void HAL_Init(void) {
    static bool init = false;

    if (!init) {
        BOARD_Init();

        AD_Init();
        PWM_Init();
        RC_Init();

        init = true;
    }
}

#include "RCServo.h"

#include <stdbool.h>

#ifndef _SUPPRESS_PLIB_WARNING
#define _SUPPRESS_PLIB_WARNING
#endif

#include <RC_Servo.h>

#include "HAL.h"

void RCServo_Init(RCServo* inst, RCServoPin pin) {
    HAL_Init();

    inst->pin = pin;
    switch (pin) {
        case RCServo_kV3:
            inst->pin = RC_PORTV03;
            break;
        case RCServo_kV4:
            inst->pin = RC_PORTV04;
            break;
        case RCServo_kW7:
            inst->pin = RC_PORTW07;
            break;
        case RCServo_kW8:
            inst->pin = RC_PORTW08;
            break;
        case RCServo_kY6:
            inst->pin = RC_PORTY06;
            break;
        case RCServo_kY7:
            inst->pin = RC_PORTY07;
            break;
        case RCServo_kZ8:
            inst->pin = RC_PORTZ08;
            break;
        case RCServo_kZ9:
            inst->pin = RC_PORTZ09;
            break;
    }

    RC_AddPins(inst->pin);
}

void RCServo_Destroy(RCServo* inst) { RC_RemovePins(inst->pin); }

void RCServo_Set(RCServo* inst, int position) {
    RC_SetPulseTime(
            inst->pin, MINPULSE + position * (MAXPULSE - MINPULSE) / 1000);
}

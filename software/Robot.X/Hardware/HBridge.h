#ifndef CMPE118_PROJECT_ROBOT_X_HARDWARE_HBRIDGE_H_
#define CMPE118_PROJECT_ROBOT_X_HARDWARE_HBRIDGE_H_

#include <stdbool.h>
#include <stdint.h>

#include "DIODefs.h"
#include "DigitalOutput.h"

/**
 * Call HAL_Init() before initializing this class.
 */
typedef struct {
    uint8_t pwmPin;
    DigitalOutput direction;
    bool isInverted;
} HBridge;

typedef enum { PWM_kX11, PWM_kY4, PWM_kY10, PWM_kY12, PWM_kZ6 } PWMPin;

/**
 * Initialize H bridge on the given PWM pin and digital output pin.
 *
 * @param inst          HBridge instance
 * @param pwmPin        PWM signal pin of motor
 * @param directionPort Port for direction pin of H bridge
 * @param directionPin  Pin on direction port
 */
void HBridge_Init(
        HBridge* inst, PWMPin pwmPin, Port directionPort, Pin directionPin);

/**
 * Destroy H bridge.
 */
void HBridge_Destroy(HBridge* inst);

/**
 * Set speed of H bridge.
 *
 * @param inst  HBridge instance
 * @param speed -1000 for full reverse and 1000 for full forward
 */
void HBridge_Set(HBridge* inst, int32_t speed);

/**
 * @param inst    HBridge instance
 * @param voltage Voltage at which to drive the H bridge (-9000..9000 for
 *                -9V..9V).
 */
void HBridge_SetVoltage(HBridge* inst, int32_t voltage);

/**
 * Set whether H bridge should invert speed settings.
 *
 * @param inst       HBridge instance
 * @param isInverted True for inverted speed settings
 */
void HBridge_SetInverted(HBridge* inst, bool isInverted);

#endif // CMPE118_PROJECT_ROBOT_X_HARDWARE_HBRIDGE_H_

#include "HBridge.h"

#include <stdlib.h>

#include <pwm.h>

#include "AnalogInput.h"

#ifndef _SUPPRESS_PLIB_WARNING
#define _SUPPRESS_PLIB_WARNING
#endif

#include "HAL.h"

static int32_t clamp(int32_t value, int32_t low, int32_t high) {
    return max(low, min(value, high));
}

void HBridge_Init(
        HBridge* inst, PWMPin pwmPin, Port directionPort, Pin directionPin) {
    HAL_Init();

    switch (pwmPin) {
        case PWM_kX11:
            inst->pwmPin = PWM_PORTX11;
            break;
        case PWM_kY4:
            inst->pwmPin = PWM_PORTY04;
            break;
        case PWM_kY10:
            inst->pwmPin = PWM_PORTY10;
            break;
        case PWM_kY12:
            inst->pwmPin = PWM_PORTY12;
            break;
        case PWM_kZ6:
            inst->pwmPin = PWM_PORTZ06;
            break;
    }

    PWM_AddPins(inst->pwmPin);
    PWM_SetFrequency(500);

    DigitalOutput_Init(&inst->direction, directionPort, directionPin);

    inst->isInverted = false;
}

void HBridge_Destroy(HBridge* inst) { PWM_RemovePins(inst->pwmPin); }

void HBridge_Set(HBridge* inst, int32_t speed) {
    int32_t absSpeed = abs(speed);

    if (speed > 0 && !inst->isInverted || speed < 0 && inst->isInverted) {
        DigitalOutput_Set(&inst->direction, true);
    } else {
        DigitalOutput_Set(&inst->direction, false);
    }

    PWM_SetDutyCycle(inst->pwmPin, clamp(absSpeed, 0, 1000));
}

void HBridge_SetVoltage(HBridge* inst, int32_t voltage) {
    HBridge_Set(
            inst, voltage * 1000 / (int32_t)AnalogInput_GetBatteryVoltage());
}

void HBridge_SetInverted(HBridge* inst, bool isInverted) {
    inst->isInverted = isInverted;
}

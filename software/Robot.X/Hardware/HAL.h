#ifndef CMPE118_PROJECT_ROBOT_X_HARDWARE_HAL_H_
#define CMPE118_PROJECT_ROBOT_X_HARDWARE_HAL_H_

/**
 * Initialize HAL subsystems.
 */
void HAL_Init(void);

#endif // CMPE118_PROJECT_ROBOT_X_HARDWARE_HAL_H_

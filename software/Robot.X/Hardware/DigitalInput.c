#include "DigitalInput.h"

#ifndef _SUPPRESS_PLIB_WARNING
#define _SUPPRESS_PLIB_WARNING
#endif

#include <IO_Ports.h>

#include "HAL.h"

void DigitalInput_Init(DigitalInput* inst, Port port, Pin pin) {
    HAL_Init();

    inst->port = port;
    inst->pin = pin;

    IO_PortsSetPortInputs(port, pin);
}

bool DigitalInput_Get(DigitalInput* inst) {
    return (IO_PortsReadPort(inst->port) & inst->pin) != 0;
}

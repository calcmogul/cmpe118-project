#ifndef CMPE118_PROJECT_ROBOT_X_SM_FOLLOWTAPECWSM_H_
#define CMPE118_PROJECT_ROBOT_X_SM_FOLLOWTAPECWSM_H_

#include "../ES_Configure.h"

#include <stdint.h>

#include <ES_Events.h>

// Service base class
uint8_t FollowTapeCWSM_Service_Init(uint8_t priority);
uint8_t FollowTapeCWSM_Service_Post(ES_Event event);
ES_Event FollowTapeCWSM_Service_Run(ES_Event event);

#endif // CMPE118_PROJECT_ROBOT_X_SM_FOLLOWTAPECWSM_H_

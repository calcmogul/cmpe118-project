#include "FollowTapeCWSM.h"

#include <stdbool.h>

#include <ES_Framework.h>
#include <ES_TattleTale.h>

#include "../EventCheckers/TapeSensorChecker.h"
#include "../Subsystems/DifferentialDrive.h"

typedef enum {
    kInit, // Initial state
    kForward,
    kFirstRotation,
    kSecondRotation,
    kTurnLeft,
    kPivotRight,
} FollowTapeCWSMState_t;

static const char* StateNames[] = {
    "kInit",
    "kForward",
    "kFirstRotation",
    "kSecondRotation",
    "kTurnLeft",
    "kPivotRight",
};

static FollowTapeCWSMState_t state = kInit;
static uint8_t m_priority;

uint8_t FollowTapeCWSM_Service_Init(uint8_t priority) {
    ES_Event event;

    state = kInit;
    m_priority = priority;

    // Post the initial transition event
    event.EventType = ES_INIT;
    return ES_PostToService(m_priority, event);
}

uint8_t FollowTapeCWSM_Service_Post(ES_Event event) {
    return ES_PostToService(m_priority, event);
}

ES_Event FollowTapeCWSM_Service_Run(ES_Event event) {
    bool makeTransition = false;
    FollowTapeCWSMState_t nextState;

#ifdef USE_TATTLETALE
    ES_AddTattlePoint(__FUNCTION__, StateNames[state], event);
#endif

    switch (state) {
        case kInit:
            switch (event.EventType) {
                case ES_INIT:
                    nextState = kTurnLeft;
                    makeTransition = true;
                    break;
            }
            break;
        case kTurnLeft:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(720, 900);
                    break;
                case EVENT_TAPESENSOR:
                    if (event.EventParam & kTapeRight) {
                        nextState = kPivotRight;
                        makeTransition = true;
                    }
                    event.EventType = ES_NO_EVENT;
                    break;
            }
            break;
        case kPivotRight:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(810, -810);
                    break;
                case EVENT_TAPESENSOR:
                    if ((event.EventParam & kTapeRight) == 0) {
                        nextState = kTurnLeft;
                        makeTransition = true;
                    }
                    event.EventType = ES_NO_EVENT;
                    break;
            }
            break;
    }

    if (makeTransition) {
        FollowTapeCWSM_Service_Run((ES_Event){ ES_EXIT, 0 });
        state = nextState;
        FollowTapeCWSM_Service_Run((ES_Event){ ES_ENTRY, 0 });
    }

#ifdef USE_TATTLETALE
    ES_Tail(); // trace call stack end
#endif

    return event;
}

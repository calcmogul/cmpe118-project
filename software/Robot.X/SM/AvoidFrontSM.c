#include "AvoidFrontSM.h"

#include <stdbool.h>

#include <ES_Framework.h>
#include <ES_TattleTale.h>

#include "../EventCheckers/BeaconDetectorChecker.h"
#include "../EventCheckers/BumperChecker.h"
#include "../EventCheckers/TapeSensorChecker.h"
#include "../Subsystems/DifferentialDrive.h"
#include "../TimerDefs.h"
#include "Hardware/AnalogInput.h"
#include "SM/MainSM.h"

typedef enum {
    kInit, // Initial state
    kDriveBackward,
    kFirstRotation,
    kDriveIn,
    kSecondRotation,
    kDriveRen,
    kWaitForElevator,
    kShootRenShip,
    kShootDriveBy,
    kDriveAcross,
    kThirdRotation,
    kBackToTape,
} AvoidFrontSMState_t;

static const char* StateNames[] = {
    "kInit",
    "kDriveBackward",
    "kFirstRotation",
    "kDriveIn",
    "kSecondRotation",
    "kDriveRen",
    "kWaitForElevator",
    "kShootRenShip",
    "kShootDriveBy",
    "kDriveAcross",
    "kThirdRotation",
    "kBackToTape",
};

static AvoidFrontSMState_t state = kInit;
static uint8_t m_priority;

uint8_t AvoidFrontSM_Service_Init(uint8_t priority) {
    ES_Event event;

    state = kInit;
    m_priority = priority;

    // Post the initial transition event
    event.EventType = ES_INIT;
    return ES_PostToService(m_priority, event);
}

uint8_t AvoidFrontSM_Service_Post(ES_Event event) {
    return ES_PostToService(m_priority, event);
}

ES_Event AvoidFrontSM_Service_Run(ES_Event event) {
    static bool foundBeacon = false;

    const uint32_t kRotateCW90PeriodMs = 475;
    const uint32_t kRotateCCW90PeriodMs = 450;
    const uint32_t kDriveBackwardPeriodMs = 300;
    const uint32_t kDriveInRenPeriodMs = 415;
    const uint32_t kDriveInAvoidPeriodMs = 650;
    const uint32_t kDriveAcrossRenPeriodMs = 700;
    const uint32_t kDriveAcrossAvoidPeriodMs = 1600;

    bool makeTransition = false;
    AvoidFrontSMState_t nextState;

#ifdef USE_TATTLETALE
    ES_AddTattlePoint(__FUNCTION__, StateNames[state], event);
#endif

    switch (state) {
        case kInit:
            switch (event.EventType) {
                case ES_INIT:
                    foundBeacon = BeaconDetectorChecker_Get();
                    nextState = kDriveBackward;
                    makeTransition = true;
                    break;
            }
            break;
        case kDriveBackward:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(-1000, -1000);
                    ES_Timer_InitTimer(TIMER_MAINSM, kDriveBackwardPeriodMs);
                    break;
                case ES_EXIT:
                    DifferentialDrive_Drive(0, 0);
                    ES_Timer_StopTimer(TIMER_MAINSM);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_MAINSM) {
                        nextState = kFirstRotation;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
        case kFirstRotation:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(1000, -1000);
                    ES_Timer_InitTimer(TIMER_MAINSM, kRotateCW90PeriodMs);
                    break;
                case ES_EXIT:
                    DifferentialDrive_Drive(0, 0);
                    ES_Timer_StopTimer(TIMER_MAINSM);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_MAINSM) {
                        nextState = kDriveIn;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
        case kDriveIn:
            switch (event.EventType) {
                case ES_ENTRY:
                    // Drive forward Ren Ship depth + 1/2 robot width
                    DifferentialDrive_Drive(1000, 1000);
                    if (foundBeacon) {
                        ES_Timer_InitTimer(TIMER_MAINSM, kDriveInRenPeriodMs);
                    } else {
                        ES_Timer_InitTimer(TIMER_MAINSM, kDriveInAvoidPeriodMs);
                    }
                    break;
                case ES_EXIT:
                    DifferentialDrive_Drive(0, 0);
                    ES_Timer_StopTimer(TIMER_MAINSM);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_MAINSM) {
                        nextState = kSecondRotation;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
        case kSecondRotation:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(-1000, 1000);
                    ES_Timer_InitTimer(TIMER_MAINSM, kRotateCCW90PeriodMs);
                    break;
                case ES_EXIT:
                    DifferentialDrive_Drive(0, 0);
                    ES_Timer_StopTimer(TIMER_MAINSM);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_MAINSM) {
                        if (foundBeacon) {
                            nextState = kDriveRen;
                        } else {
                            nextState = kDriveAcross;
                        }
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
        case kDriveRen:
            switch (event.EventType) {
                case ES_ENTRY:
                    // Drive forward for half the Ren Ship width
                    DifferentialDrive_Drive(1000, 1000);
                    ES_Timer_InitTimer(TIMER_MAINSM, kDriveAcrossRenPeriodMs);
                    break;
                case ES_EXIT:
                    DifferentialDrive_Drive(0, 0);
                    ES_Timer_StopTimer(TIMER_MAINSM);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_MAINSM) {
                        nextState = kShootRenShip;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
        case kShootRenShip:
            switch (event.EventType) {
                case ES_ENTRY:
                    Shooter_Service_Post((ES_Event){ CMD_SHOOTER_SHOOT, 0 });
                    break;
                case DONE_SHOOTER_SHOOT:
                    nextState = kShootDriveBy;
                    makeTransition = true;
                    event.EventType = ES_NO_EVENT;
                    break;
            }
            break;
        case kShootDriveBy:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(1000, 1000);
                    ES_Timer_InitTimer(TIMER_MAINSM, 50);
                    break;
                case ES_EXIT:
                    DifferentialDrive_Drive(0, 0);
                    ES_Timer_StopTimer(TIMER_MAINSM);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_MAINSM) {
                        nextState = kShootRenShip;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
        case kDriveAcross:
            switch (event.EventType) {
                case ES_ENTRY:
                    // Drive forward for the Ren Ship width
                    DifferentialDrive_Drive(1000, 1000);
                    ES_Timer_InitTimer(TIMER_MAINSM, kDriveAcrossAvoidPeriodMs);
                    break;
                case ES_EXIT:
                    DifferentialDrive_Drive(0, 0);
                    ES_Timer_StopTimer(TIMER_MAINSM);
                    break;
                case EVENT_BUMPER:
                    if (event.EventParam & kBumperFrontLeft
                            || event.EventParam & kBumperFrontRight) {
                        nextState = kDriveBackward;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
                case EVENT_TAPESENSOR:
                    if (event.EventParam & kTapeFront) {
                        // Go back to normal SM line following
                        MainSM_Service_Post((ES_Event){ DONE_AVOIDFRONTSM, 0 });
                    }
                    break;
            }
            break;
        case kThirdRotation:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(-1000, 1000);
                    ES_Timer_InitTimer(TIMER_MAINSM, kRotateCCW90PeriodMs);
                    break;
                case ES_EXIT:
                    DifferentialDrive_Drive(0, 0);
                    ES_Timer_StopTimer(TIMER_MAINSM);
                    break;
                case ES_TIMEOUT:
                    if (event.EventParam == TIMER_MAINSM) {
                        nextState = kBackToTape;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
                case EVENT_TAPESENSOR:
                    if (event.EventParam & kTapeFront) {
                        // Go back to normal SM line following
                        MainSM_Service_Post((ES_Event){ DONE_AVOIDFRONTSM, 0 });
                    }
                    break;
            }
            break;
        case kBackToTape:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(1000, 1000);
                    break;
                case EVENT_TAPESENSOR:
                    if (event.EventParam & kTapeFront) {
                        // Go back to normal SM line following
                        MainSM_Service_Post((ES_Event){ DONE_AVOIDFRONTSM, 0 });
                    }
                    break;
            }
            break;
    }

    if (makeTransition) {
        AvoidFrontSM_Service_Run((ES_Event){ ES_EXIT, 0 });
        state = nextState;
        AvoidFrontSM_Service_Run((ES_Event){ ES_ENTRY, 0 });
    }

#ifdef USE_TATTLETALE
    ES_Tail(); // trace call stack end
#endif

    return event;
}

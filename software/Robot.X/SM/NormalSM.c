#include "NormalSM.h"

#include <stdbool.h>

#include <ES_Framework.h>
#include <ES_TattleTale.h>

#include "../EventCheckers/BumperChecker.h"
#include "../Subsystems/DifferentialDrive.h"
#include "../Subsystems/Shooter.h"
#include "../TimerDefs.h"
#include "FollowTapeCWSM.h"

typedef enum {
    kInit, // Initial state
    kForward,
    kFollowTapeCWSM,
    kShootATM6,
} NormalSMState_t;

static const char* StateNames[] = {
    "kInit",
    "kForward",
    "kFollowTapeCWSM",
    "kShootATM6",
};

static NormalSMState_t state = kInit;
static uint8_t m_priority;

uint8_t NormalSM_Service_Init(uint8_t priority) {
    ES_Event event;

    state = kInit;
    m_priority = priority;

    // Post the initial transition event
    event.EventType = ES_INIT;
    return ES_PostToService(m_priority, event);
}

uint8_t NormalSM_Service_Post(ES_Event event) {
    return ES_PostToService(m_priority, event);
}

ES_Event NormalSM_Service_Run(ES_Event event) {
    bool makeTransition = false;
    NormalSMState_t nextState;

#ifdef USE_TATTLETALE
    ES_AddTattlePoint(__FUNCTION__, StateNames[state], event);
#endif

    switch (state) {
        case kInit:
            switch (event.EventType) {
                case ES_INIT:
                    nextState = kFollowTapeCWSM;
                    makeTransition = true;
                    break;
            }
            break;
        case kFollowTapeCWSM:
            event = FollowTapeCWSM_Service_Run(event);

            switch (event.EventType) {
                case EVENT_TRACKWIRE_ON:
                    DifferentialDrive_Drive(-1000, -1000);
                    nextState = kShootATM6;
                    makeTransition = true;
                    break;
            }
            break;
        case kShootATM6:
            switch (event.EventType) {
                case ES_ENTRY:
                    DifferentialDrive_Drive(0, 0);
                    Shooter_Service_Post((ES_Event){ CMD_SHOOTER_SHOOT, 0 });
                    break;
                case DONE_SHOOTER_SHOOT:
                    nextState = kFollowTapeCWSM;
                    makeTransition = true;
                    event.EventType = ES_NO_EVENT;
                    break;
            }
            break;
    }

    if (makeTransition) {
        if (nextState == kFollowTapeCWSM) {
            FollowTapeCWSM_Service_Init(m_priority);
        }

        NormalSM_Service_Run((ES_Event){ ES_EXIT, 0 });
        state = nextState;
        NormalSM_Service_Run((ES_Event){ ES_ENTRY, 0 });
    }

#ifdef USE_TATTLETALE
    ES_Tail(); // trace call stack end
#endif

    return event;
}

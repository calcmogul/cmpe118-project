#include "MainSM.h"

#include <stdbool.h>

#include <ES_Framework.h>
#include <ES_TattleTale.h>

#include "../EventCheckers/BumperChecker.h"
#include "AvoidFrontSM.h"
#include "NormalSM.h"
#include "Subsystems/Shooter.h"

typedef enum {
    kInit, // Initial state
    kNormalSM,
    kAvoidFrontSM,
} MainSMState_t;

static const char* StateNames[] = {
    "kInit",
    "kNormalSM",
    "kAvoidFrontSM",
};

static MainSMState_t state = kInit;
static uint8_t m_priority;

uint8_t MainSM_Service_Init(uint8_t priority) {
    ES_Event event;

    state = kInit;
    m_priority = priority;

    // Post the initial transition event
    event.EventType = ES_INIT;
    return ES_PostToService(m_priority, event);
}

uint8_t MainSM_Service_Post(ES_Event event) {
    return ES_PostToService(m_priority, event);
}

ES_Event MainSM_Service_Run(ES_Event event) {
    bool makeTransition = false;
    MainSMState_t nextState;

#ifdef USE_TATTLETALE
    ES_AddTattlePoint(__FUNCTION__, StateNames[state], event);
#endif

    switch (state) {
        case kInit:
            switch (event.EventType) {
                case ES_INIT:
                    nextState = kNormalSM;
                    makeTransition = true;
                    break;
            }
            break;
        case kNormalSM:
            event = NormalSM_Service_Run(event);

            switch (event.EventType) {
                case EVENT_BUMPER:
                    if (event.EventParam & kBumperFrontLeft
                            || event.EventParam & kBumperFrontRight) {
                        nextState = kAvoidFrontSM;
                        makeTransition = true;
                        event.EventType = ES_NO_EVENT;
                    }
                    break;
            }
            break;
        case kAvoidFrontSM:
            event = AvoidFrontSM_Service_Run(event);

            switch (event.EventType) {
                case DONE_AVOIDFRONTSM:
                    nextState = kNormalSM;
                    makeTransition = true;
                    event.EventType = ES_NO_EVENT;
                    break;
            }
            break;
    }

    if (makeTransition) {
        if (nextState == kNormalSM) {
            NormalSM_Service_Init(m_priority);
        } else if (nextState == kAvoidFrontSM) {
            AvoidFrontSM_Service_Init(m_priority);
        }

        MainSM_Service_Run((ES_Event){ ES_EXIT, 0 });
        state = nextState;
        MainSM_Service_Run((ES_Event){ ES_ENTRY, 0 });
    }

#ifdef USE_TATTLETALE
    ES_Tail(); // trace call stack end
#endif

    return event;
}

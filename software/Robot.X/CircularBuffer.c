#include "CircularBuffer.h"

#include <string.h>

void CircularBuffer_Init(CircularBuffer* inst) {
    memset(inst->buffer, 0, sizeof(inst->buffer));
    inst->index = 0;
    inst->size = 0;
}

void CircularBuffer_PushBack(CircularBuffer* inst, uint32_t value) {
    inst->buffer[inst->index] = value;
    inst->index = (inst->index + 1) % FILTER_SIZE;
    if (inst->size < FILTER_SIZE) {
        inst->size++;
    }
}

uint32_t CircularBuffer_GetAverage(CircularBuffer* inst) {
    uint32_t sum = 0;
    for (int i = 0; i < inst->size; i++) {
        sum += inst->buffer[(inst->index + i) % FILTER_SIZE];
    }
    return sum / FILTER_SIZE;
}

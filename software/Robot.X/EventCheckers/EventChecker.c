#include "EventChecker.h"

#include "BeaconDetectorChecker.h"
#include "BumperChecker.h"
#include "TapeSensorChecker.h"
#include "TrackWireSensorChecker.h"

uint8_t EventChecker_Check(void) {
    bool detectedEvent = false;

    detectedEvent |= BeaconDetectorChecker_EventChecker_Check();
    detectedEvent |= BumperChecker_EventChecker_Check();
    detectedEvent |= TapeSensorChecker_EventChecker_Check();
    detectedEvent |= TrackWireSensorChecker_EventChecker_Check();

    return detectedEvent;
}

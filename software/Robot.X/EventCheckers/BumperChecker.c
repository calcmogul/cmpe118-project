#include "../ES_Configure.h"
#include "BumperChecker.h"

#include <stdbool.h>

#include <ES_Framework.h>

#include "../Hardware/DigitalInput.h"
#include "../SM/MainSM.h"
#include "../Subsystems/Elevator.h"

static DigitalInput frontLeftBumper;
static DigitalInput frontRightBumper;

static uint16_t flBumperPrev = 0;
static uint16_t frBumperPrev = 0;

uint8_t BumperChecker_GetBumpers(void) {
    uint8_t bumpers = 0;

    if (flBumperPrev == 0xFFFF) {
        bumpers |= kBumperFrontLeft;
    }
    if (frBumperPrev == 0xFFFF) {
        bumpers |= kBumperFrontRight;
    }

    return bumpers;
}

bool BumperChecker_EventChecker_Check(void) {
    static bool init = false;
    if (!init) {
        DigitalInput_Init(&frontLeftBumper, kPortZ, kPin8);
        DigitalInput_Init(&frontRightBumper, kPortZ, kPin9);
        init = true;
    }

    static uint16_t flBumperNow = 0;
    static uint16_t frBumperNow = 0;

    flBumperNow = flBumperNow << 1 | DigitalInput_Get(&frontLeftBumper);
    frBumperNow = frBumperNow << 1 | DigitalInput_Get(&frontRightBumper);

    ES_Event event = { EVENT_BUMPER, 0 };
    bool postEvent = false;

    if (flBumperPrev == 0 && flBumperNow == 0xFFFF) { // low to high
        flBumperPrev = 0xFFFF;
        event.EventParam |= kBumperFrontLeft;
        postEvent = true;
    } else if (flBumperPrev == 0xFFFF && flBumperNow == 0) { // high to low
        flBumperPrev = 0;
        postEvent = true;
    }

    if (frBumperPrev == 0 && frBumperNow == 0xFFFF) { // low to high
        frBumperPrev = 0xFFFF;
        event.EventParam |= kBumperFrontRight;
        postEvent = true;
    } else if (frBumperPrev == 0xFFFF && frBumperNow == 0) { // high to low
        frBumperPrev = 0;
        postEvent = true;
    }

    if (postEvent) {
#ifndef USE_KEYBOARD_INPUT
        MainSM_Service_Post(event);
        Elevator_Service_Post(event);
#endif
    }

    return postEvent;
}

#ifndef CMPE118_PROJECT_ROBOT_X_EVENTCHECKERS_BEACONDETECTORCHECKER_H_
#define CMPE118_PROJECT_ROBOT_X_EVENTCHECKERS_BEACONDETECTORCHECKER_H_

#include <stdbool.h>

/**
 * Returns true if beacon is within range.
 */
bool BeaconDetectorChecker_Get(void);

// EventChecker base class
bool BeaconDetectorChecker_EventChecker_Check(void);

#endif // CMPE118_PROJECT_ROBOT_X_EVENTCHECKERS_BEACONDETECTORCHECKER_H_

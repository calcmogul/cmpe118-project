#ifndef CMPE118_PROJECT_ROBOT_X_EVENTCHECKERS_BUMPERCHECKER_H_
#define CMPE118_PROJECT_ROBOT_X_EVENTCHECKERS_BUMPERCHECKER_H_

#include <stdbool.h>
#include <stdint.h>

typedef enum { kBumperFrontLeft = 0x1, kBumperFrontRight = 0x2 } BumperType;

/**
 * Returns last latched state of all bumpers.
 */
uint8_t BumperChecker_GetBumpers(void);

bool BumperChecker_EventChecker_Check(void);

#endif // CMPE118_PROJECT_ROBOT_X_EVENTCHECKERS_BUMPERCHECKER_H_

#include "../ES_Configure.h"
#include "TrackWireSensorChecker.h"

#include <ES_Framework.h>

#include "../CircularBuffer.h"
#include "../Hardware/AnalogInput.h"
#include "../SM/MainSM.h"
#include "../Subsystems/Shooter.h"

static AnalogInput trackWire;
static CircularBuffer filter;

bool TrackWireSensorChecker_EventChecker_Check(void) {
    static bool init = false;
    if (!init) {
        AnalogInput_Init(&trackWire, AnalogInput_kW3);
        CircularBuffer_Init(&filter);
        init = true;
    }

    // Threshold voltages
    const uint32_t kTrackWireLowThreshold = 1800;
    const uint32_t kTrackWireHighThreshold = 2400;

    static ES_EventTyp_t lastEventType = EVENT_TRACKWIRE_OFF;
    static uint32_t sample = 0;

    // Only check for events with new ADC data
    if (!AnalogInput_IsNewDataReady() || Shooter_IsRunning()) {
        return false;
    }

    sample = AnalogInput_GetVoltage(&trackWire);
    CircularBuffer_PushBack(&filter, sample);
    sample = CircularBuffer_GetAverage(&filter);

    if (sample > kTrackWireHighThreshold
            && lastEventType == EVENT_TRACKWIRE_OFF) {
#ifndef USE_KEYBOARD_INPUT
        MainSM_Service_Post((ES_Event){ EVENT_TRACKWIRE_ON, 0 });
#endif
        lastEventType = EVENT_TRACKWIRE_ON;
        return true;
    } else if (sample < kTrackWireLowThreshold
            && lastEventType == EVENT_TRACKWIRE_ON) {
#ifndef USE_KEYBOARD_INPUT
        MainSM_Service_Post((ES_Event){ EVENT_TRACKWIRE_OFF, 0 });
#endif
        lastEventType = EVENT_TRACKWIRE_OFF;
        return true;
    }

    return false;
}

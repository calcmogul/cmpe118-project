#include "TapeSensorChecker.h"

#include <ES_Framework.h>

#include "../Hardware/AnalogInput.h"
#include "../Hardware/DigitalOutput.h"
#include "../SM/MainSM.h"
#include "../Subsystems/Shooter.h"

static uint8_t m_priority;

static AnalogInput leftTape;
static AnalogInput frontTape;
static AnalogInput rightTape;
static DigitalOutput tapeLED;

static uint32_t matchStartTime = 0;

/**
 * Returns true if tape event occurred.
 *
 * @param prevLowSample     Previous sample with LED off
 * @param prevHighSample    Previous sample with LED on
 * @param currentLowSample  Current sample with LED off
 * @param currentHighSample Current sample with LED on
 * @param onTape            Stores whether on or off tape
 */
static bool Check(uint32_t prevLowSample, uint32_t prevHighSample,
        uint32_t currentLowSample, uint32_t currentHighSample, bool* onTape);

bool TapeSensorChecker_EventChecker_Check(void) {
    static bool init = false;
    if (!init) {
        AnalogInput_Init(&leftTape, AnalogInput_kV3);
        AnalogInput_Init(&frontTape, AnalogInput_kV4);
        AnalogInput_Init(&rightTape, AnalogInput_kV5);
        DigitalOutput_Init(&tapeLED, kPortV, kPin7);
        DigitalOutput_Set(&tapeLED, true);
        init = true;
    }

    // Previous sensor readings
    static uint32_t prevLeftLowSample = 0;
    static uint32_t prevFrontLowSample = 0;
    static uint32_t prevRightLowSample = 0;
    static uint32_t prevLeftHighSample = 0;
    static uint32_t prevFrontHighSample = 0;
    static uint32_t prevRightHighSample = 0;

    // Current sensor readings
    static uint32_t currentLeftLowSample = 0;
    static uint32_t currentFrontLowSample = 0;
    static uint32_t currentRightLowSample = 0;
    static uint32_t currentLeftHighSample = 0;
    static uint32_t currentFrontHighSample = 0;
    static uint32_t currentRightHighSample = 0;

    // Don't post events if the shooter is running because
    // the shooter causes a lot of noise.
    if (!Shooter_IsRunning()) {
        currentLeftHighSample = AnalogInput_Get(&leftTape);
        currentFrontHighSample = AnalogInput_Get(&frontTape);
        currentRightHighSample = AnalogInput_Get(&rightTape);

        ES_Event newEvent = { EVENT_TAPESENSOR, 0 };
        bool onTape = false;
        bool postEvent = false;

        // Check for left event
        postEvent |= Check(prevLeftLowSample, prevLeftHighSample,
                currentLeftLowSample, currentLeftHighSample, &onTape);
        if (onTape) {
            newEvent.EventParam |= kTapeLeft;
        }
        prevLeftHighSample = currentLeftHighSample;
        prevLeftLowSample = currentLeftLowSample;

        // Check for front event
        postEvent |= Check(prevFrontLowSample, prevFrontHighSample,
                currentFrontLowSample, currentFrontHighSample, &onTape);
        if (onTape) {
            newEvent.EventParam |= kTapeFront;
        }
        prevFrontHighSample = currentFrontHighSample;
        prevFrontLowSample = currentFrontLowSample;

        // Check for right event
        postEvent |= Check(prevRightLowSample, prevRightHighSample,
                currentRightLowSample, currentRightHighSample, &onTape);
        if (onTape) {
            newEvent.EventParam |= kTapeRight;
        }
        prevRightHighSample = currentRightHighSample;
        prevRightLowSample = currentRightLowSample;

        if (postEvent) {
#ifndef USE_KEYBOARD_INPUT
            MainSM_Service_Post(newEvent);
#endif
            return true;
        }
    }

    return false;
}

static bool Check(uint32_t prevLowSample, uint32_t prevHighSample,
        uint32_t currentLowSample, uint32_t currentHighSample, bool* onTape) {
    // Less than threshold means on tape
    const uint32_t kTapeThreshold = 300;

    // Start match timer if power was just turned on
    if (prevHighSample == 0 && currentHighSample != 0) {
        matchStartTime = ES_Timer_GetTime();
    }

    // If any "LED on" ADC readings are 0, power is off and sensor readings are
    // invalid
    if (currentHighSample == 0) {
        *onTape = false;
        return false;
    }

    // If "LED on" sample was less than "LED off" sample, raise high sample
    if (prevHighSample < prevLowSample) {
        prevHighSample = prevLowSample;
    }
    if (currentHighSample < currentLowSample) {
        currentHighSample = currentLowSample;
    }

    bool prevOnTape = prevHighSample < kTapeThreshold;
    bool currentOnTape = currentHighSample < kTapeThreshold;
    *onTape = currentOnTape;

    return prevOnTape && !currentOnTape || !prevOnTape && currentOnTape;
}

uint32_t GetMatchTime(void) { return ES_Timer_GetTime() - matchStartTime; }

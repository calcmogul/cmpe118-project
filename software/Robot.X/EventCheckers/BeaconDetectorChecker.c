#include "../ES_Configure.h"

#include <ES_Framework.h>

#include "../Hardware/AnalogInput.h"
#include "../SM/MainSM.h"
#include "TapeSensorChecker.h"

typedef enum { kOn, kOff } BeaconDetectorState;

static AnalogInput beaconDetector;
static BeaconDetectorState state = kOff;

bool BeaconDetectorChecker_Get(void) { return state == kOn; }

bool BeaconDetectorChecker_EventChecker_Check(void) {
    static bool init = false;
    if (!init) {
        AnalogInput_Init(&beaconDetector, AnalogInput_kW8);
        init = true;
    }

    static const uint32_t lowThreshold = 70;
    static const uint32_t highThreshold = 90;

    uint32_t sample = AnalogInput_Get(&beaconDetector);
    bool postedEvent = false;

    if (state == kOff && sample > highThreshold) {
        state = kOn;
        postedEvent = true;
#ifndef USE_KEYBOARD_INPUT
        MainSM_Service_Post((ES_Event){ EVENT_BEACONDETECTOR_ON, 0 });
#endif
    } else if (state == kOn && sample < lowThreshold) {
        state = kOff;
        postedEvent = true;
#ifndef USE_KEYBOARD_INPUT
        MainSM_Service_Post((ES_Event){ EVENT_BEACONDETECTOR_OFF, 0 });
#endif
    }

    return postedEvent;
}

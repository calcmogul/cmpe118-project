#ifndef CMPE118_PROJECT_ROBOT_X_EVENTCHECKERS_TAPESENSORCHECKER_H_
#define CMPE118_PROJECT_ROBOT_X_EVENTCHECKERS_TAPESENSORCHECKER_H_

#include "../ES_Configure.h"

#include <stdbool.h>
#include <stdint.h>

#include <ES_Events.h>

typedef enum {
    kTapeLeft = 0x1,
    kTapeFront = 0x2,
    kTapeRight = 0x4
} TapeSensorType;

// EventChecker base class
bool TapeSensorChecker_EventChecker_Check(void);

/**
 * Returns milliseconds since start of match or 0 if match hasn't started yet.
 *
 * The match starts when the power switches from off to on. This can be measured
 * using the tape sensors because they read 0 when the power is off and non-zero
 * otherwise.
 */
uint32_t GetMatchTime(void);

#endif // CMPE118_PROJECT_ROBOT_X_EVENTCHECKERS_TAPESENSORCHECKER_H_

#ifndef CMPE118_PROJECT_ROBOT_X_CIRCULARBUFFER_H_
#define CMPE118_PROJECT_ROBOT_X_CIRCULARBUFFER_H_

#include <stdint.h>

#define FILTER_SIZE 16

typedef struct {
    uint32_t buffer[FILTER_SIZE];
    uint32_t index;
    uint32_t size;
} CircularBuffer;

/**
 * Initialize circular buffer.
 *
 * @param inst CircularBuffer instance.
 */
void CircularBuffer_Init(CircularBuffer* inst);

/**
 * Pushes new value into circular buffer. This overwrites the oldest value if
 * the buffer is full.
 *
 * @param inst  CircularBuffer instance.
 * @param value Value to push into the circular buffer.
 */
void CircularBuffer_PushBack(CircularBuffer* inst, uint32_t value);

/**
 * Returns average of values within buffer.
 *
 * @param inst CircularBuffer instance.
 */
uint32_t CircularBuffer_GetAverage(CircularBuffer* inst);

#endif // CMPE118_PROJECT_ROBOT_X_CIRCULARBUFFER_H_

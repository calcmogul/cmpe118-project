#include "ES_Configure.h"

#include <stdbool.h>
#include <stdio.h>

#include <ES_Framework.h>

#include "Hardware/HAL.h"
#include "Hardware/HBridge.h"
#include "Subsystems/DifferentialDrive.h"
#include "Subsystems/Elevator.h"
#include "Subsystems/Shooter.h"

int main(void) {
    ES_Return_t ErrorType;

    HAL_Init();

    HBridge leftMotor;
    HBridge_Init(&leftMotor, PWM_kY12, kPortZ, kPin4);

    HBridge rightMotor;
    HBridge_Init(&rightMotor, PWM_kZ6, kPortZ, kPin5);
    HBridge_SetInverted(&rightMotor, true);

    DifferentialDrive drive;
    DifferentialDrive_Init(&drive, &leftMotor, &rightMotor);

    HBridge elevatorMotor;
    HBridge_Init(&elevatorMotor, PWM_kY10, kPortY, kPin8);

    Elevator elevator;
    Elevator_Init(&elevator, &elevatorMotor);

    HBridge flywheel;
    HBridge_Init(&flywheel, PWM_kY4, kPortY, kPin6);

    RCServo indexer;
    RCServo_Init(&indexer, RCServo_kW7);

    Shooter shooter;
    Shooter_Init(&shooter, &flywheel, &indexer);

    printf("Starting ES Framework Template\r\n");
    printf("using the 2nd Generation Events & Services Framework\r\n");

    // now initialize the Events and Services Framework and start it running
    ErrorType = ES_Initialize();
    if (ErrorType == Success) {
        ErrorType = ES_Run();
    }

    // if we got to here, there was an error
    switch (ErrorType) {
        case FailedPointer:
            printf("Failed on NULL pointer");
            break;
        case FailedInit:
            printf("Failed Initialization");
            break;
        default:
            printf("Other Failure: %d", ErrorType);
            break;
    }

    while (1) {
    }
};

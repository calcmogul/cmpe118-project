import os
import sys
import zipfile
import requests
import shutil
import subprocess
import zlib


def main():
    toDir = './'
    sourceURL = 'http://classes.soe.ucsc.edu/cmpe118/Fall17/Labs/CMPE118.zip'

    rawZip = requests.get(sourceURL, stream=True)
    zipPath = os.path.join(os.sys.path[0], 'cmpe118.zip')

    # We currently write this to a file and read it back in, we should be able
    # to do it directly in memory instead
    with open(zipPath, 'wb') as f:
        rawZip.raw.decode_content = True
        shutil.copyfileobj(rawZip.raw, f)

    zip = zipfile.ZipFile(zipPath)

    # Iterate all the files to completely mirror the structure
    filesInZip = []
    for fileInfo in zip.infolist():
        # Convert Windows file paths to Unix
        localFilePath = '/'.join(
            os.path.join(toDir, fileInfo.filename).split('/'))

        filesInZip.append(localFilePath)
        localDir, _ = os.path.split(localFilePath)
        if not os.path.exists(localDir):
            try:
                os.makedirs(localDir)
            except:
                print('Cannot build directory structure, exiting')
                input()
                sys.exit(1)
            print('Creating ' + localDir)
        try:
            curCRC = zlib.crc32(open(localFilePath, 'rb').read())
            if curCRC != fileInfo.CRC:
                print('Replacing ' + localFilePath)
                zip.extract(fileInfo, toDir)
        except FileNotFoundError:
            # If destination file not found, move instead of copy.
            zip.extract(fileInfo, toDir)
    zip.close()

    for root, dir, files in os.walk(toDir + 'CMPE118'):
        for file in files:
            curFilePath = os.path.join(root, file)

            # Compare against zip
            if not curFilePath in filesInZip:
                # Confirm that still in base directory for massive error
                # prevention
                if toDir + 'CMPE118' in curFilePath:
                    print('File not found: ' + curFilePath)
                    try:
                        print('Trying to remove' + curFilePath)
                        os.remove(curFilePath)
                    except:
                        print('Unable to remove: ' + curFilePath)
                        sys.exit(1)

    for root, dir, files in os.walk(toDir + 'CMPE118', topdown=False):
        for name in dir:
            try:
                os.rmdir(os.path.join(root, name))
            except OSError:
                pass
            except Exception as e:
                print("Unknown error in folder deletion")
                print(e)
                sys.exit(1)

    # Delete .log files
    files = [
        os.path.join(dp, f)
        for dp, dn, fn in os.walk(".")
        for f in fn
        if f.endswith(".log")
    ]
    for f in files:
        os.remove(f)

    # Apply external patches
    print("Applying patches")
    subprocess.run(["git", "apply", "cmpe118-fixes.patch"])

    cmd = "find CMPE118 -type f -regextype posix-extended " + \
          "-regex '.*\.(c|h|ld)$' -exec dos2unix {} \;"
    subprocess.run(cmd, shell=True)
    sys.exit(0)


if __name__ == "__main__":
    main()

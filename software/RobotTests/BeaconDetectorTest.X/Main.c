#include <stdio.h>

#include <timers.h>

#include "Hardware/AnalogInput.h"

int main(void) {
    AnalogInput leftTrackWire;
    AnalogInput_Init(&leftTrackWire, AnalogInput_kW3);

    AnalogInput rightTrackWire;
    AnalogInput_Init(&rightTrackWire, AnalogInput_kW4);

    TIMERS_Init();
    TIMERS_InitTimer(0, 500);

    while (1) {
        if (TIMERS_IsTimerExpired(0) == TIMER_EXPIRED) {
            int32_t value = AnalogInput_Get(&leftTrackWire);
            printf("left=%i\r\n", value);

            value = AnalogInput_Get(&rightTrackWire);
            printf("right=%i\r\n", value);

            // Reset timer and clear event
            TIMERS_InitTimer(0, 500);
        }
    }
}

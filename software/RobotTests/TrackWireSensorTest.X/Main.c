#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <timers.h>

#include "Hardware/AnalogInput.h"
#include "Hardware/HBridge.h"

int main(void) {
    AnalogInput trackWire;
    AnalogInput_Init(&trackWire, AnalogInput_kW3);

    HBridge flywheel;
    HBridge_Init(&flywheel, PWM_kY4, kPortY, kPin6);
    HBridge_Set(&flywheel, 0);

    TIMERS_Init();
    TIMERS_InitTimer(0, 2);

    while (1) {
        if (TIMERS_IsTimerExpired(0) == TIMER_EXPIRED) {
            uint32_t sample = AnalogInput_Get(&trackWire);
            printf("%i\r\n", sample);

            // Reset timer and clear event
            TIMERS_InitTimer(0, 2);
        }
    }
}

#include <stdbool.h>
#include <stdio.h>

#include <timers.h>

#include "Hardware/HBridge.h"
#include "Hardware/RCServo.h"

int main(void) {
    RCServo indexer;
    RCServo_Init(&indexer, RCServo_kW7);

    HBridge flywheel;
    HBridge_Init(&flywheel, PWM_kY4, kPortY, kPin6);
    HBridge_Set(&flywheel, -1000);

    TIMERS_Init();
    TIMERS_InitTimer(0, 2000);
    bool driveForward = true;

    while (1) {
        if (TIMERS_IsTimerExpired(0) == TIMER_EXPIRED) {
            if (driveForward) {
                RCServo_Set(&indexer, 1000);
                driveForward = false;
                printf("forward\r\n");
            } else {
                RCServo_Set(&indexer, 0);
                driveForward = true;
                printf("backward\r\n");
            }

            // Reset timer and clear event
            TIMERS_InitTimer(0, 2000);
        }
    }
}

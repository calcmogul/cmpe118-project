#include <stdbool.h>
#include <stdio.h>

#include <timers.h>

#include "Hardware/HBridge.h"

int main(void) {
    HBridge leftMotor;
    HBridge_Init(&leftMotor, PWM_kY12, kPortZ, kPin4);

    HBridge rightMotor;
    HBridge_Init(&rightMotor, PWM_kZ6, kPortZ, kPin5);
    HBridge_SetInverted(&rightMotor, true);

    HBridge flywheel;
    HBridge_Init(&flywheel, PWM_kY4, kPortY, kPin6);
    HBridge_Set(&flywheel, 0);

    TIMERS_Init();
    TIMERS_InitTimer(0, 1000);
    bool driveForward = true;

    while (1) {
        if (TIMERS_IsTimerExpired(0) == TIMER_EXPIRED) {
            if (driveForward) {
                HBridge_Set(&leftMotor, 1000);
                HBridge_Set(&rightMotor, 1000);
                driveForward = false;
                printf("Forward\r\n");
            } else {
                HBridge_Set(&leftMotor, -1000);
                HBridge_Set(&rightMotor, -1000);
                driveForward = true;
                printf("Backward\r\n");
            }

            // Reset timer and clear event
            TIMERS_InitTimer(0, 1000);
        }
    }
}

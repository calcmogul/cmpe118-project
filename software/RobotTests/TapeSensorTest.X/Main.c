#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <timers.h>

#include "Hardware/AnalogInput.h"
#include "Hardware/DigitalOutput.h"
#include "Hardware/HBridge.h"

int main(void) {
    AnalogInput leftTape;
    AnalogInput_Init(&leftTape, AnalogInput_kV4);

    AnalogInput frontTape;
    AnalogInput_Init(&frontTape, AnalogInput_kV4);

    AnalogInput rightTape;
    AnalogInput_Init(&rightTape, AnalogInput_kV5);

    DigitalOutput tapeLED;
    DigitalOutput_Init(&tapeLED, kPortV, kPin7);
    DigitalOutput_Set(&tapeLED, false);

    HBridge flywheel;
    HBridge_Init(&flywheel, PWM_kY4, kPortY, kPin6);
    HBridge_Set(&flywheel, 0);

    TIMERS_Init();
    TIMERS_InitTimer(0, 2);
    bool ledOn = false;
    int32_t leftVal;
    int32_t frontVal;
    int32_t rightVal;

    while (1) {
        if (TIMERS_IsTimerExpired(0) == TIMER_EXPIRED) {
            if (ledOn) {
                leftVal = AnalogInput_Get(&leftTape);
                frontVal = AnalogInput_Get(&frontTape);
                rightVal = AnalogInput_Get(&rightTape);
                printf("%3i %3i %3i", leftVal, frontVal, rightVal);

                ledOn = false;
                DigitalOutput_Set(&tapeLED, ledOn);
            } else {
                leftVal = AnalogInput_Get(&leftTape);
                frontVal = AnalogInput_Get(&frontTape);
                rightVal = AnalogInput_Get(&rightTape);
                printf("%3i %3i %3i\r\n", leftVal, frontVal, rightVal);

                ledOn = true;
                DigitalOutput_Set(&tapeLED, ledOn);
            }

            // Reset timer and clear event
            TIMERS_InitTimer(0, 2);
        }
    }
}

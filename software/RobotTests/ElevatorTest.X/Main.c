#include <stdbool.h>
#include <stdio.h>

#include <timers.h>

#include "Hardware/HBridge.h"

int main(void) {
    const int kTimeout = 15500;

    HBridge elevatorMotor;
    HBridge_Init(&elevatorMotor, PWM_kY10, kPortY, kPin8);
    HBridge_Set(&elevatorMotor, -1000);

    HBridge flywheel;
    HBridge_Init(&flywheel, PWM_kY4, kPortY, kPin6);
    HBridge_Set(&flywheel, 0);

    TIMERS_Init();
    TIMERS_InitTimer(0, kTimeout);
    bool goUp = true;

    printf("up\r\n");

    while (1) {
        if (TIMERS_IsTimerExpired(0) == TIMER_EXPIRED) {
            if (goUp) {
                HBridge_Set(&elevatorMotor, 1000);
                goUp = false;
                printf("down\r\n");
            } else {
                HBridge_Set(&elevatorMotor, -1000);
                goUp = true;
                printf("up\r\n");
            }

            // Reset timer and clear event
            TIMERS_InitTimer(0, kTimeout);
        }
    }
}

#include <stdio.h>

#include <BOARD.h>
#include <timers.h>

int main(void) {
    BOARD_Init();

    TIMERS_Init();
    TIMERS_InitTimer(0, 500);

    while (1) {
        if (TIMERS_IsTimerExpired(0) == TIMER_EXPIRED) {
            printf("Hello World!\r\n");

            // Reset timer and clear event
            TIMERS_InitTimer(0, 500);
        }
    }
}

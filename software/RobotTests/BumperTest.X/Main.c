#include <stdbool.h>
#include <stdio.h>

#include <timers.h>

#include "Hardware/DigitalInput.h"

int main(void) {
    DigitalInput leftBumper;
    DigitalInput_Init(&leftBumper, kPortZ, kPin8);

    DigitalInput rightBumper;
    DigitalInput_Init(&rightBumper, kPortZ, kPin9);

    TIMERS_Init();
    TIMERS_InitTimer(0, 500);

    while (1) {
        if (TIMERS_IsTimerExpired(0) == TIMER_EXPIRED) {
            bool value = DigitalInput_Get(&leftBumper);
            printf("left=%i\r\n", value);

            value = DigitalInput_Get(&rightBumper);
            printf("right=%i\r\n", value);

            // Reset timer and clear event
            TIMERS_InitTimer(0, 500);
        }
    }
}

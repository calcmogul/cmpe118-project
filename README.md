This repository uses [wpiformat](https://github.com/wpilibsuite/styleguide/tree/master/wpiformat) to enforce its style guide. See wpiformat's README for installation instructions.

`update_cmpe118.py` pulls the latest version of the CMPE118 E&S framework files,
then applies the patches in `cmpe118-fixes.patch` to make it build on non-lab
Windows environments and Linux.

Python 3 is required to build the software for this project.
